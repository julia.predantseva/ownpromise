const state = Symbol('state');
const value = Symbol('value');
const onReject = Symbol('onReject');
const onResolve = Symbol('onResolve');
const callBackCall = Symbol('callBackCall');
const registerResult = Symbol('registerResult');
const arrOfCallbacks = Symbol('arrOfCallbacks');
const registerCallbacks = Symbol('registerCallbacks');

class OwnPromise {
  constructor(cB) {
    this[state] = '<pending>';
    this[value] = undefined;
    this[arrOfCallbacks] = [];

    if (typeof cB !== 'function') {
      throw new TypeError();
    }
    try {
      cB(this[onResolve], this[onReject]);
    } catch (error) {
      this[onReject](error);
    }
  }
  [onResolve] = value => {
    if (this.constructor !== OwnPromise) {
      throw new TypeError();
    }

    if (this[state] !== '<pending>') {
      return;
    }

    this[state] = 'resolved';
    this[registerResult](value);
  }
  [onReject] = error => {
    if (this.constructor !== OwnPromise) {
      throw new TypeError();
    }

    if (this[state] !== '<pending>') {
      return;
    }
    this[state] = 'rejected';
    this[registerResult](error);
  }
  [registerResult] = result => {
    this[value] = result;
    setTimeout(() => {
      this[arrOfCallbacks].forEach(cb => {
        this[callBackCall](cb.onFulfilled, cb.onRejected);
      });
    }, 0);
  }
  [callBackCall] = (onFulfilled, onRejected) => {
    if (typeof onFulfilled !== 'function' || typeof onRejected !== 'function') {
      throw this[value];
    }

    if (this[state] === 'resolved') {
      onFulfilled(this[value]);
      return;
    }
    onRejected(this[value]);
  }
  [registerCallbacks] = (onFulfilled, onRejected) => {
    if (this[state] === '<pending>') {
      this[arrOfCallbacks].push({ onFulfilled, onRejected });
      return;
    }
    setTimeout(() => {
      this[callBackCall](onFulfilled, onRejected);
    });
  }
  then(onFulfilled, onReject) {
    if (this.constructor !== OwnPromise) {
      throw new TypeError();
    }

    return new OwnPromise((res, rej) => {
      this[registerCallbacks](
        value => {
          if (typeof onFulfilled !== 'function') {
            res(value);
            return;
          }
          res(onFulfilled(value));
        },
        error => {
          if (typeof onReject !== 'function') {
            rej(error);
            return;
          }
          rej(onReject(error));
        });
    });
  }
  catch(reject) {
    if (typeof reject !== 'function') {
      return;
    }

    return this.then(null, reject);
  }
  static resolve(value) {
    if (this.constructor !== OwnPromise && this !== OwnPromise) {
      throw new TypeError();
    }

    if (value instanceof OwnPromise) {
      return value;
    }

    return new OwnPromise(res => res(value));
  }
  static reject(error) {
    if (this.constructor !== OwnPromise && this !== OwnPromise) {
      throw new TypeError();
    }

    return new OwnPromise((_, rej) => rej(error));
  }
  static all(promises) {
    if (this.constructor !== OwnPromise && this !== OwnPromise) {
      throw new TypeError();
    }

    if (typeof promises[Symbol.iterator] !== 'function') {
      return new OwnPromise((_, rej) => rej(new TypeError()));
    }

    if (promises.length === 0) {
      return new OwnPromise(res => res([]));
    }

    return new OwnPromise((res, rej) => {
      const promisesResultArr = [];

      for (let promise of promises) {
        if (!(promise instanceof OwnPromise)) {
          promise = OwnPromise.resolve(promise);
        }
        promise.then(value => promisesResultArr.push(value), err => rej(err))
          .then(() => {
            if (promisesResultArr.length === promises.length) {
              res(promisesResultArr);
            }
          });
      }
    });
  }
  static race(promises) {
    if (this.constructor !== OwnPromise && this !== OwnPromise) {
      throw new TypeError();
    }

    if (typeof promises[Symbol.iterator] !== 'function') {
      return new OwnPromise((_, rej) => rej(new TypeError()));
    }

    return new OwnPromise((res, rej) => {
      for (const promise of promises) {
        promise.then(res, rej);
      }
    });
  }
}

module.exports = OwnPromise;
